# International Trainings Course Seismology and Seismic Hazard - Moment Tensor Inversion

## Lecturers

* Simone Cesca <cesca@gfz-potsdam.de>
* Pinar Büyükakpinar <pinar@gfz-potsdam.de>
* Gesa Petersen <gesap@gfz-potsdam.de>
* Daniela Kuehn <daniela@norsar.no>
* Sebastian Heimann <sebastian.heimann@uni-potsdam.de>

## Chat

Please join the [itc2023
channel](https://hive.pyrocko.org/pyrocko-support/channels/itc2023) on the
[Pyrocko Hive](https://hive.pyrocko.org).

## Program

**Thursday, June 29**

* 08:30 - 10:00: Moment Tensor Inversion - Theory
* 10:30 - 12:00: Earthquake Data, Agencies and Formats
* 13:30 - 15:00: Data Access, Preparation and Visualization
* 15:30 - 17:00: Green’s Functions

**Friday, June 30**

* 08:30 - 10:00: Synthetic Seismograms
* 10:30 - 12:00: Moment Tensor Inversion with Grond
* 13:30 - 15:00: Moment Tensor Inversion Exercise I
* 15:30 - 17:00: Moment Tensor Inversion Exercise II

## Software

Some special software is needed to perform our exercises.

**Option 1: Use our preconfigured virtual machine**

Download the following VM image and import it into [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

* [itc2023.ova](https://data.pyrocko.org/itc/itc2023.ova) - VM image with all software pre-installed (3.5 GB)

Choose *Import Appliance* from the *File* menu and select the downloaded VM
image. Then start the VM. The password is `earthquake101` but it is configured
that you don't have to enter it.

In the guest system, open a terminal and type

```
itc-update
```

to get the exercise dataset and software hot-fixes.

**Option 2: Use your own computer**

Install the following software on your Linux or Mac machine (Windows may
also work but is not recommended):

* [Pyrocko](https://pyrocko.org/docs/current/install/index.html)
* [Grond](https://pyrocko.org/grond/docs/current/install/index.html)
* [QSEIS Backend for Fomosto](https://git.pyrocko.org/pyrocko/fomosto-qseis)

## Presentations

* [Source inversion in seismology](https://data.pyrocko.org/itc/2023/presentations/cesca_itc2023potsdam_sourceinversion.pdf)
* [The World of Seismological Data](https://data.pyrocko.org/itc/2023/presentations/lecture-data-new/)
* [Pyrocko Squirrel](https://data.pyrocko.org/itc/2023/presentations/squirrel-2022/)
* [Pyrocko Snuffler](https://data.pyrocko.org/presentations/pyrocko-workshop-dgg-2021/#/snuffler)
* [Pyrocko GF](https://data.pyrocko.org/itc/2023/presentations/presentation-gf.pdf)
* [Grond - The practical side](https://data.pyrocko.org/itc/2023/presentations/grond-intro)

## Links

* [Pyrocko web page](https://pyrocko.org)
* [Pyrocko manual](https://pyrocko.org/docs/current/)
* [Grond manual](https://pyrocko.org/grond)
* [Snuffler manual](https://pyrocko.org/docs/current/apps/snuffler/index.html)
* [Squirrel command line tool tutorial](https://pyrocko.org/docs/current/apps/squirrel/tutorial.html)
* [Pyrocko GF overview](https://pyrocko.org/docs/current/topics/pyrocko-gf.html)


## Do your own inversion:

* find an event (M 5-6) in the [Geofon catalog](https://geofon.gfz-potsdam.de/eqinfo/list.php)

* find your crust2 velocity model:
```
from pyrocko import crust2x2 

print(crust2x2.get_profile(lat=event_latitude, lon=event_longitude))

```

* ```grond init example_regional_cmt my_first_grond_project```

* ```cd my_first_grond_project```

* ```mkdir gf_stores```

* ```cd gf_stores```

* download gf_store: ```fomosto download crust2_j3``` 

* ```cd ..```

* ```gedit bin/grondown``` --> change Line 1 pyrocko-python to python3

* run grondown: ```bin/grondown_regional.sh eventname```

